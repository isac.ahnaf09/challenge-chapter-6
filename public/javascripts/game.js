const dataComOption = ["rock", "paper", "scissors"]
let hasGameStarted = false;
let hasSaved = false;


class Game{
    constructor(){
        this.choice = "";
    }

    //memberikan class on-focus untuk menandai pilihan user dan pilihan computer
    highlightTheChoice(userChoice, comChoice){
        document.getElementById('user-'+userChoice).classList.add('on-focus');
        document.getElementById('com-'+comChoice).classList.add('on-focus');
    }

    //menentukan pemenang dengan membandingkan pilihan user dan pilihan computer
    whoIsTheWinner(userChoice, comChoice){
        let resultTheWinner;

        if(userChoice == comChoice){
            return resultTheWinner = "draw";
        }else{
            if(userChoice == "rock" && comChoice == "paper" || userChoice == "scissors" && comChoice == "rock" || userChoice == "paper" && comChoice == "scissors"){
                return resultTheWinner = "com win";
            }else if(userChoice == "rock" && comChoice == "scissors" || userChoice == "scissors" && comChoice == "paper" || userChoice == "paper" && comChoice == "rock"){
                return resultTheWinner = "user win";
            }else{
                return resultTheWinner = "error";
            }
        }
    }

    //memberikan display untuk hasil game, dan memberikan value untuk disimpan ke dalam database
    gameResult(theWinnerIs){
        document.getElementById('beforeGameStarted').style.display = 'none';
        
        if(theWinnerIs == 'com win'){
            document.getElementById('com-win').classList.add('result-display')//ini untuk tampilan ke user
            document.getElementById('gameResult').value = "LOSE"//ini untuk value yang disimpan ke dalam database
        }else if(theWinnerIs == 'user win'){
            document.getElementById('user-win').classList.add('result-display')
            document.getElementById('gameResult').value = "WIN"
        }else{
            document.getElementById('draw').classList.add('result-display')
            document.getElementById('gameResult').value = "DRAW"
        }
    }

    //reset game
    resetTheGame(){
        //mereset hasGameStarted == false and hasSaved = false
        hasGameStarted = false;
        hasSaved  = false;
    
        let highlightTheChoice = document.getElementsByClassName('on-focus')
        let resetGame = document.getElementsByClassName('result-display')
    
        //menghapus highlight pada user choice dan com choice
        highlightTheChoice[0].classList.remove('on-focus')    
        highlightTheChoice[0].classList.remove('on-focus')
    
        //reset gameResult
        document.getElementById('beforeGameStarted').style.display = 'block';
        resetGame[0].classList.remove('result-display')
        
    }
}

class UserSide extends Game{
    constructor(userPick){
        super()
        this.choice = userPick
    }

    userPick(){
        return this.choice
    }
}   

class ComSide extends Game{
    constructor(){
        super();
    }

    //mendapatakan pilihan computer dengan mengacak menggunakan Math.random
    whatComChoice(){
        let comPick = Math.floor(Math.random() * 3);
        const comChoice = dataComOption[comPick]; 
    
        return comChoice;
    }
}

const user = new UserSide;
const com = new ComSide;

//mendapatkan nilai dari pilihan user dan mendapatkan hasil game
function whatUserChoice(userPick){
    let userChoice = userPick;
    let comChoice = com.whatComChoice();
    
    //mendapatkan pemenang dari hasil whoIsTheWinner
    let winner = user.whoIsTheWinner(userChoice, comChoice);

    //hasGameStarted harus bernilai false karena game tidak dapat dimainkan ketika sudah dimulai, dan harus di-reset untuk dapat memainkannya lagi
    if(hasGameStarted == false){
        hasGameStarted = true;
        user.highlightTheChoice(userPick,comChoice);
        user.gameResult(winner);
    }
}

//menyimpan hasil permainan ke dalam database
const saveGameResult =  async(username) => {
    let gameResult = document.getElementById('gameResult').value

    //nilai awal hasSaved harus bernilai false dan di set menjadi true ketika sudah tersimpan untuk memastikan agar tidak ada redundancy data
    if(hasSaved == false){
        const resp = await fetch(`http://localhost:3000/game/${username}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                status : gameResult
            })
        })
    }

    if(resp.status === 201){
        console.log("success")
        hasSaved = true;
    }else{
        console.log("error")
    }
}

//mereset game ketika sudah selesai dan ingin memainkannya kembali
function refreshButton(){
    if(hasGameStarted == true){
        user.resetTheGame();
    }
}