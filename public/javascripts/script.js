//===========LOGIN HANDLER===========

const loginHandler = async () => {
    let username = document.getElementById("loginUsername").value
    let password = document.getElementById("loginPassword").value

    //memastikan username dan password sudah terisi sebelum dikirim ke endpoint api
    if(username == "" || password == ""){
      alert("please insert username and password correctly")
    }else{
      //memanggil endpoint login dan mengirimkan value username untuk dicari apakah user tersebut ada atau tidak (username bersifat unique username)
      const resp = await fetch('http://localhost:3000/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username,
          password
        })
      })

      if(resp.status === 200){
        //mendapat response berupa data username dan password user
        const data = await resp.json()

          if(data.password == password){//hanya membandingkan password karena ketika username tidak ditemukan maka otomatis akan masuk ke kondisi berikutnya
              window.location.replace(`/users/${data.username}`)
          }else if(data.adminPassword == password){//ketika username = admin maka api akan mengirimkan password admin lalu di bandingkan dengan inputan user
            window.location.replace('/admin')
          }else{
              alert("username or password are incorrect")
          }
      }else if(resp.status === 404){//response status ketika user tidak ditemukan
          alert("Username not found") 
      }
    }
}

//===========END LOGIN HANDLER===========


//======REGISTER HANDLER========
const handleRegister = async () => {
    let username = document.getElementById("username").value
    let password = document.getElementById("password").value
    let email = document.getElementById("email").value
    let name = document.getElementById("fullname").value
    let address = document.getElementById("address").value
    let gender = document.querySelector('.form-group select').value
    
    if(username == "" || password == "" || email == ""){//memastikan 3 field ini harus atau not null
      alert("please insert username, password, and email correctly")
    }else{
      //memanggil endpoint signup untuk selanjutnya inputan user disimpan ke dalam database
      const resp = await fetch('http://localhost:3000/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username,
          password,
          email,
          name,
          address,
          gender
        })
      })
    
      if(resp.status === 201){
        alert("Signup is success")
        window.location.replace('/login')
      }else if (resp.status === 409){
        alert("username already exist, please use another one")
      }
    }
}

const updateProfilePrepare = async (id) => {
  const trEl = document.getElementById(id)

  document.getElementById("username").value = trEl.children[0].innerText
  document.getElementById("password").value = trEl.children[1].innerText
  document.getElementById("email").value = trEl.children[2].innerText
  document.getElementById("fullname").value = trEl.children[3].innerText
  document.getElementById("address").value = trEl.children[4].innerText
  document.getElementById("gender").value = trEl.children[5].innerText

  document.getElementById("editForm").style.display="block";
  document.getElementById("editButton").disabled = true;
  document.getElementById("username").disabled = true;
}

const updateProfileHandler = async (username) => {
  let password = document.getElementById("password").value
  let email = document.getElementById("email").value
  let name = document.getElementById("fullname").value
  let address = document.getElementById("address").value
  let gender = document.getElementById("gender").value


  const resp = await fetch(`http://localhost:3000/users/profile/${username}`, {
    method : 'PUT',
    headers : {
      'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
      password,
      email,
      name,
      address,
      gender
    })
  })

  if(resp.status === 202){
    alert("Data Successfully Updated")
    location.reload()
  }else{
    alert("Failed Update data")
  }
}

//======= GAME HANDLER ========
const playGame = async (username) =>{
  window.location.href = `http://localhost:3000/game/${username}`
}


//======== ADMIN HANDLER =========
const addUserHanlder = async () => {
  let username = document.getElementById("username").value
  let password = document.getElementById("password").value
  let email = document.getElementById("email").value
  let name = document.getElementById("fullname").value
  let address = document.getElementById("address").value
  let gender = document.querySelector('.form-group select').value
  
  if(username == "" || password == "" || email == ""){
    alert("please insert username, password, and email correctly")
  }else{
    const resp = await fetch('http://localhost:3000/admin/add-user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password,
        email,
        name,
        address,
        gender
      })
    })

    if(resp.status === 201){
      alert("Data is successfully added")
      window.location.reload()
    }else if (resp.status === 409){
      alert("username already exist, please use another one")
    }
  }
}

const prepareEditHandler = async (id) => {
  const trEl = document.getElementById(id)

  document.getElementById("username").value = trEl.children[1].innerText
  document.getElementById("password").value = trEl.children[2].innerText
  document.getElementById("email").value = trEl.children[3].innerText
  document.getElementById("fullname").value = trEl.children[4].innerText
  document.getElementById("address").value = trEl.children[5].innerText
  document.getElementById("gender").value = trEl.children[6].innerText

  let username = document.getElementById("username").value

  document.getElementById("btnEdit").style.display="block";
  document.getElementById("btnEdit").setAttribute('onclick', `updateUserHandler("${username}")`)
  document.getElementById("btnInsert").style.display="none";
  document.getElementById("username").disabled = true;
}

const updateUserHandler = async (username) => {
  let password = document.getElementById("password").value
  let email = document.getElementById("email").value
  let name = document.getElementById("fullname").value
  let address = document.getElementById("address").value
  let gender = document.getElementById("gender").value


  const resp = await fetch(`http://localhost:3000/admin/update-user/${username}`, {
    method : 'PUT',
    headers : {
      'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
      password,
      email,
      name,
      address,
      gender
    })
  })

  if(resp.status === 202){
    alert("Data Successfully Updated")
    window.location.reload()
  }else{
    alert("Failed Update data")
  }
}

const deleteUserHanlder = async (id) => {
  let answer = confirm('Are you sure?')
  if (answer){
    const resp = await fetch(`http://localhost:3000/admin/delete-user/${id}`, {
      method: 'DELETE'
    })

    if(resp.status === 202){
      alert("Data Successfully Deleted")
      window.location.reload()
    }else{
      alert("Failed to delete data")
    }
  }
}


//========LOGOUT HANDLER=========
const logoutHandle = async () =>{
  window.location.replace('http://localhost:3000/login')
}