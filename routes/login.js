var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const { UserGame, UserGameBiodata } = require('../models')
const superUser = {
    adminPassword : "admin"
}
const jsonParser = bodyParser.json()


/* endpoint untuk merender tampilan login */
router.get('/', function(req, res, next) {
    res.render('login')
});

/* endpoint untuk memvalidasi kecocokan inputan user dengan data pada database */
router.post('/', async (req, res, next) => {
    try {
        const dataUser = await UserGame.findOne({
            where : {
                username : req.body.username
            }
        })

        if(dataUser){
            res.status(200).send(dataUser)
        }else if(req.body.username === "admin"){
            res.status(200).send(superUser)
        }else{
            res.status(404).send('user not found')
        }

    } catch (error) {
        res.send('error')
    }
});


module.exports = router;
