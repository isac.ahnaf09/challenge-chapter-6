var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const { UserGame, UserGameBiodata } = require('../models');
const app = require('../app');

const jsonParser = bodyParser.json()

/* endpoint ini digunakan untuk menampilkan list users pada dashboard admin pada menu Users */
router.get('/users', async (req, res) => {
  try {
    const dataUser = await UserGame.findAll({
      include : UserGameBiodata
    });

    res.render('dashboard-admin-users', {dataUser})

  } catch (error) {
    res.status(501).send("internal server error")
  }
})

/* enpoint ini digunakan untuk merender tampilan dashboard admin */
router.get('/', (req, res) => {
  try {
    res.render('dashboard-admin')
  } catch (error) {
    res.status(500).send("internal server error")
  }
})

/* endpoint ini digunakan untuk menambahkan user oleh admin */
router.post('/add-user', jsonParser, async (req, res, next) =>{
  try {
    //insert into table UserGame
    const dataUserGame = await UserGame.create({
      username : req.body.username,
      password : req.body.password
    })
    //insert into table UserGameBiodata
    const dataUserGameBiodata = await UserGameBiodata.create({
      name : req.body.name,
      address : req.body.address,
      email : req.body.email,
      gender : req.body.gender,
      UserGameId : dataUserGame.id
    })

    res.status(201).send('Done insert user')

  } catch (error) {
    if(error.message == "Validation error"){//karena username bersifat unique maka ketika ada yang duplikasi server akan mengirimkan error.message yaitu Validation error
      res.status(409).send("duplicate username")
    }else{
      res.status(500).send(error.message)
    }
  }
})

/* endpoint ini digunakan untuk update data user oleh admin */
router.put('/update-user/:username', async (req, res) => {
  try {
    const data = await UserGame.findOne({
      where: {
        username : req.params.username
      }
    })
    const dataBiodata = await UserGameBiodata.findOne({
      where : {
        UserGameId : data.id
      }
    })
  
    data.password = req.body.password
    dataBiodata.email = req.body.email
    dataBiodata.name = req.body.name
    dataBiodata.address = req.body.address
    dataBiodata.gender = req.body.gender
    dataBiodata.updateAt = new Date()
  
    data.save()
    dataBiodata.save()
    
    res.status(202).send('data has been edited')
  } catch (error) {
    res.status(500).send("internal server error")
  }
})

/* endpoint ini digunkana untuk menghapus data user oleh admin */
router.delete('/delete-user/:id', async(req,res) => {
  try {
    const data = await UserGame.findByPk(req.params.id)
    data.destroy()
    res.status(202).send('DELETED')
  } catch (error) {
    res.status(500).send("internal server error")
  }
})

module.exports = router;
