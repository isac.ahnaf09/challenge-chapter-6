var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const { UserGame, UserGameBiodata } = require('../models')

const jsonParser = bodyParser.json()

/* endpoint untuk merender tampilan signup */
router.get('/', function(req, res, next) {
    res.render('signup')
});

//endpoint untuk menyimpan hasil inputan user ke dalam database
router.post('/', jsonParser, async (req, res, next) => {
    try {
        //insert into table userGame
        const dataUserGame = await UserGame.create({
            username : req.body.username,
            password : req.body.password
        })

        //insert into table userGameBiodata
        const dataUserGameBiodata = await UserGameBiodata.create({
            name : req.body.name,
            address : req.body.address,
            email : req.body.email,
            UserGameId : dataUserGame.id,
            gender : req.body.gender
        })

        res.status(201).send('Successfully Register')

    } catch (error) {
        if(error.message == "Validation error"){//karena username bersifat unique maka ketika ada yang duplikasi server akan mengirimkan error.message yaitu Validation error
            res.status(409).send("duplicate username")
        }else{
            res.status(500).send(error.message)
        }
    }
});



module.exports = router;
