var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const { UserGame, UserGameHistory } = require('../models');
const jsonParser = bodyParser.json()


//merender view game bedasarkan username
router.get('/:username', async (req, res, next) => {
    try {
        const dataUser = await UserGame.findOne({
            where : {
                username : req.params.username
            }
        })
    
        res.render('game', {dataUser})
    } catch (error) {
        res.status(500).send("internal server error : " + error)
    }
})

//menyimpan hasil permainan ke dalam database sebagai history permainan
router.post('/:username', jsonParser, async (req, res) => {
    try {
        //read dataUser
        const dataUser = await UserGame.findOne({
            where:{
                username : req.params.username
            }
        })

        //insert dataGameHistory
        const dataGameHistory = await UserGameHistory.create({
            status : req.body.status,
            UserGameId : dataUser.id
        })

        res.status(201).send("history has been saved")
    } catch (error) {
        res.status(500).send("internal server error : " + error)
    }
})

//mengambil data history pemain untuk ditampilkan pada menu statistik
router.get('/history/:username', jsonParser, async (req, res) => {
    try {
        //read dataUser
        const dataUser = await UserGame.findOne({
            where:{
                username : req.params.username
            }
        })

        //insert dataGameHistory
        const dataGameHistory = await UserGameHistory.findAll({
            where : {
                UserGameId : dataUser.id
            }
        })

        res.render('dashboard-user-history', {dataGameHistory, data : dataUser})
    } catch (error) {
        res.status(500).send("internal server error : " + error)
    }
})

//mengambil data history semua pemain yang akan ditampilkan untuk admin pada menu statistik 
router.get('/all/history', jsonParser, async (req, res) => {
    try {
        const dataGameHistory = await UserGameHistory.findAll({
            include : UserGame
        })

        res.render('dashboard-admin-statistik', {dataGameHistory})
    } catch (error) {
        res.status(500).send("internal server error : " + error)
    }
})

module.exports = router;