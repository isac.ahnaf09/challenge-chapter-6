var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const { UserGame, UserGameBiodata } = require('../models');
const app = require('../app');
const fetch = require('node-fetch');


const jsonParser = bodyParser.json()


// //Create UserGame
// router.post('/register', jsonParser, async (req, res, next) =>{
//   try {
//     //insert into table UserGame
//   const dataUserGame = await UserGame.create({
//     username : req.body.username,
//     password : req.body.password
//   })
//   //insert into table UserGameBiodata
//   const dataUserGameBiodata = await UserGameBiodata.create({
//     name : req.body.name,
//     address : req.body.address,
//     email : req.body.email,
//     gender : req.body.gender,
//     UserGameId : dataUserGame.id
//   })

//   res.status(201).send('Done insert user')
//   } catch (error) {
    
//   }
// })

//mengambil data dan merender tampilan dashboard-user
router.get('/:username', async (req, res) => {
  try {
    const data = await UserGame.findOne({
      where: {
        username: req.params.username
      }
    })
    const dataBiodata = await UserGameBiodata.findOne({
      where : {
        UserGameId : data.id
      }
    })
  
    res.render('dashboard-user', {data, dataBiodata})
  } catch (error) {
    res.status(500).send("internal server error")
  }
})

//mengambil data user untuk selanjutnya ditampailkan di dashboard-user-profile
router.get('/profile/:username', async (req, res) => {
  try {
    const data = await UserGame.findOne({
      where: {
        username: req.params.username
      }
    })
    const dataBiodata = await UserGameBiodata.findOne({
      where : {
        UserGameId : data.id
      }
    })
  
    res.render('dashboard-user-profile', {data, dataBiodata})
  } catch (error) {
    res.status(500).send("internal server error")
  }
})

//mengupdate data user dan disimpan ke dalam database
router.put('/profile/:username', async (req, res) => {
  try {
    const data = await UserGame.findOne({
      where: {
        username : req.params.username
      }
    })
    const dataBiodata = await UserGameBiodata.findOne({
      where : {
        UserGameId : data.id
      }
    })
  
    data.password = req.body.password
    dataBiodata.email = req.body.email
    dataBiodata.name = req.body.name
    dataBiodata.address = req.body.address
    dataBiodata.gender = req.body.gender
    dataBiodata.updateAt = new Date()
  
    data.save()
    dataBiodata.save()
    
    res.status(202).send('data has been edited')
  } catch (error) {
    res.status(500).send("internal server error")
  }
})




module.exports = router;