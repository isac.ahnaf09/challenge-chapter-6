var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  try {
    res.redirect('/login')
  } catch (error) {
    res.status(500).send("internal server error : " + error)
  }
});

module.exports = router;
